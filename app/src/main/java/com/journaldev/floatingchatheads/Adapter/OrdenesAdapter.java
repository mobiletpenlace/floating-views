package com.journaldev.floatingchatheads.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.journaldev.floatingchatheads.ItemClickCallbacks;
import com.journaldev.floatingchatheads.R;

import java.util.ArrayList;
import java.util.List;

public class OrdenesAdapter extends RecyclerView.Adapter<OrdenesAdapter.OrdenHolder> {
    private List<String> mListOrdenes = new ArrayList<>();
    private ItemClickCallbacks mItemClickCallbacks;


    public OrdenesAdapter(List<String> mListOrdenes, ItemClickCallbacks mItemClickCallbacks) {
        this.mListOrdenes = mListOrdenes;
        this.mItemClickCallbacks = mItemClickCallbacks;
    }

    @NonNull
    @Override
    public OrdenHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_ordenes, viewGroup, false);
        OrdenHolder vista_holder = new OrdenHolder(layoutView);
        return vista_holder;    }

    @Override
    public void onBindViewHolder(@NonNull OrdenHolder ordenHolder, int i) {
        ordenHolder.mNombreOrden.setText(mListOrdenes.get(i));
        ordenHolder.mNombreOrden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickCallbacks.OnItemClick();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListOrdenes.size();
    }

    class OrdenHolder extends RecyclerView.ViewHolder{
        TextView mNombreOrden;

        public OrdenHolder(View itemView) {
            super(itemView);
            mNombreOrden = itemView.findViewById(R.id.mNombreOrden);
        }
    }
}
