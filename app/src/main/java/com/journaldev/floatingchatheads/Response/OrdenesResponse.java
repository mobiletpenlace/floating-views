package com.journaldev.floatingchatheads.Response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrdenesResponse {
    @SerializedName("ordenes")
    List<String> mListOrdenes  = new ArrayList<>();

    public List<String> getmListOrdenes() {
        return mListOrdenes;
    }

    public void setmListOrdenes(List<String> mListOrdenes) {
        this.mListOrdenes = mListOrdenes;
    }
}
