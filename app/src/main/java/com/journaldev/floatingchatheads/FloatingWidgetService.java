package com.journaldev.floatingchatheads;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.journaldev.floatingchatheads.Adapter.OrdenesAdapter;
import com.journaldev.floatingchatheads.Apis.ApiCliente;
import com.journaldev.floatingchatheads.Apis.EndPoints;
import com.journaldev.floatingchatheads.Response.OrdenesResponse;

import java.util.EventListener;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FloatingWidgetService extends Service implements ItemClickCallbacks , View.OnClickListener , View.OnTouchListener {

    private WindowManager mWindowManager;
    private View mFloatingView;
    private RecyclerView mListOrdenes;
    private OrdenesAdapter mOrdenesAdapter;
    private LinearLayout mRootContainer;
    private WindowManager.LayoutParams params;
    private MotionEvent mEvent;
    private RelativeLayout mContainer1;
    private RelativeLayout mContainer2;
    private ImageView mProfile;
    private int initialX ;
    private int initialY;
    private float initialTouchX;
    private float initialTouchY;

    public FloatingWidgetService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate() {
        super.onCreate();
        mFloatingView = LayoutInflater.from(this).inflate(R.layout.overlay_layout, null);
        setParams();
        setBindingViews();
    }

    private void setParams(){
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        params = new WindowManager.LayoutParams(
                300,
                300,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;        //Initially view will be added to top-left corner

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mFloatingView, params);
    }

    private void setBindingViews(){
        ImageButton mImageVoice = mFloatingView.findViewById(R.id.mImageVoice);
        ImageButton mCloseButton = mFloatingView.findViewById(R.id.mCloseButton);
        Button mOpenAppButton = mFloatingView.findViewById(R.id.mOpenAppButton);
        mListOrdenes = mFloatingView.findViewById(R.id.mListOrdenesRecyclerView);
        mRootContainer = mFloatingView.findViewById(R.id.mMainContainer);
        mContainer1 = mFloatingView.findViewById(R.id.mHeaderContainer);
        mContainer2 = mFloatingView.findViewById(R.id.mVoiceContianer);
        mProfile = mFloatingView.findViewById(R.id.profile_image);
        final TextView mTitle = mFloatingView.findViewById(R.id.mTitle);

        mProfile.setOnClickListener(this);
        mImageVoice.setOnClickListener(this);
        mCloseButton.setOnClickListener(this);
        mOpenAppButton.setOnClickListener(this);
        mProfile.findViewById(R.id.profile_image).setOnTouchListener(this);

    }

    private void configureRecyclerView(List<String> mList){
        mListOrdenes.setVisibility(View.VISIBLE);
        mOrdenesAdapter = new OrdenesAdapter(mList,this);
        mListOrdenes.setLayoutManager(new LinearLayoutManager(FloatingWidgetService.this));
        mListOrdenes.setAdapter(mOrdenesAdapter);
    }

    private boolean isViewCollapsed() {
        return true;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            if (mFloatingView != null) mWindowManager.removeView(mFloatingView);
        }catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    @Override
    public void OnItemClick(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(FloatingWidgetService.this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        }else{
            Intent intent = new Intent(FloatingWidgetService.this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mWindowManager.removeView(mFloatingView);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mCloseButton:
                mWindowManager.removeView(mFloatingView);
                break;

            case R.id.mImageVoice:
                consumingTheAPI();
                break;

            case R.id.mOpenAppButton:
                openApp();
                break;

            case R.id.profile_image:
                Log.e("Click","Touch");
                showView();
                break;
        }
    }

    private void showView(){

        ViewGroup.LayoutParams params = mRootContainer.getLayoutParams();
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.width = 1000;
        mRootContainer.setLayoutParams(params);
        mWindowManager.updateViewLayout(mRootContainer, params);
        mContainer1.setVisibility(View.VISIBLE);
        mContainer2.setVisibility(View.VISIBLE);
        mProfile.setVisibility(View.GONE);
        mRootContainer.setOnTouchListener(this);
    }

    private void openApp(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(FloatingWidgetService.this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        }else{
            Intent intent = new Intent(FloatingWidgetService.this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mWindowManager.removeView(mFloatingView);
            startActivity(intent);
        }
    }

    private void consumingTheAPI(){
        EndPoints res = ApiCliente.getCliente().create(EndPoints.class);
        Call<OrdenesResponse> call = res.getOrdenes();

        call.enqueue(new Callback<OrdenesResponse>() {
            @Override
            public void onResponse(Call<OrdenesResponse> call, Response<OrdenesResponse> response) {
                List<String> listado_sucursales = response.body().getmListOrdenes();
                configureRecyclerView(listado_sucursales);
                Log.e(":v==>", listado_sucursales.get(0));
            }

            @Override
            public void onFailure(Call<OrdenesResponse> call, Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        mEvent = event;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = params.x;
                initialY = params.y;
                initialTouchX = event.getRawX();
                initialTouchY = event.getRawY();
                return false;
            case MotionEvent.ACTION_UP:
                int Xdiff = (int) (event.getRawX() - initialTouchX);
                int Ydiff = (int) (event.getRawY() - initialTouchY);

                if (Xdiff < 10 && Ydiff < 10) {
                    if (isViewCollapsed()) {

                    }
                }
                return false;
            case MotionEvent.ACTION_MOVE:
                int xDiff = Math.round(event.getRawX() - initialTouchX);
                int yDiff = Math.round(event.getRawY() - initialTouchY);


                //Calculate the X and Y coordinates of the view.
                params.x = initialX + xDiff;
                params.y = initialY + yDiff;

                //Update the layout with new X & Y coordinates
                mWindowManager.updateViewLayout(mFloatingView, params);

                return false;
        }
        return false;
    }
}
