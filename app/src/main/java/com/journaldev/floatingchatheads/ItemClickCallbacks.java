package com.journaldev.floatingchatheads;

public interface ItemClickCallbacks  {
    void OnItemClick();
}
