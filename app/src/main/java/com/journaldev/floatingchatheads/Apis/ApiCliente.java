package com.journaldev.floatingchatheads.Apis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiCliente {
    private static String url="http://politicas.gearhostpreview.com/";
    private static Retrofit retrofit = null;

    public static Retrofit getCliente() {
        Gson gson = new GsonBuilder()
                .create();

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }

        return retrofit;
    }
}
