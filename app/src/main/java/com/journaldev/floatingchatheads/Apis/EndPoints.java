package com.journaldev.floatingchatheads.Apis;

import com.journaldev.floatingchatheads.Response.OrdenesResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EndPoints {
    @GET("PruebaWidget.php")
    Call<OrdenesResponse> getOrdenes();
}
